Megadropdown category navigation for large-up widths

**Installation**

 Add this j-flow tag to ``pages/base.master.html``

``<j:categorynavigation  AddIndexClasses="true" AddActiveClasses="true" LoadAllCategories="True" />``

**Compatible modules**

- ``cat-nav-mobile``

**Incompatible modules**

- ``cat-nav-vert-down``
- ``cat-nav-vert-right``
